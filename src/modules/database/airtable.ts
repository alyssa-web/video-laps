import Airtable from 'airtable';
import TableDef from 'airtable/lib/table';
import Record from 'airtable/lib/record';

export const base = new Airtable({
  apiKey: process.env.AIRTABLE_API_KEY,
}).base(process.env.AIRTABLE_BASE_VIDEOLAPS as string);

export class Table {
    table:TableDef
    log: (args: any) => void = console.log

    constructor(table:string) {
        this.table = base(table)
        // this.log = log.extend(table)
    }

    // getRecords = (options = {}) =>
    //     this.table.select(options).firstPage()     
    
    getRecords = async (options = {}) => {
        // const log = this.log.extend('get')
        this.log(options)

        const records = await this.table.select(options).firstPage();
        return records
    }

    getRecord = async (id: string) => {
        // const log = this.log.extend('find')
        this.log(id)

        const record = await this.table.find(id);
        return record
    }

    createRecord = async (recordData: Record[] | Record) => {
        // const log = this.log.extend('create')
        this.log(recordData)

        const records = await this.table.create(recordData);
        return records;
    }

    updateRecord = async (recordData: Record[] | Record) => {
        // const log = this.log.extend('update')
        this.log(recordData)

        const records = await this.table.create(recordData);
        return records;
    }

    deleteRecord = async (id: string) => {
        // const log = this.log.extend('delete')
        this.log(id)
        
        const records = await this.table.destroy(id);
        return records;
    }
}
