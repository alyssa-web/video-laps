import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from ".";

enum Platforms {
    YOUTUBE = 'YouTube'
}

interface VideoFields {
    Name: string;
    Date: Date;
    Id: string;
    Url: string;
    Platform: Platforms
}

export interface Video {
    id: string;
    fields: VideoFields;
    createdTime: Date;
}

const initialState =  JSON.parse(localStorage.getItem('videos') as string) || [];

const videosSlice = createSlice({
    name: 'videos',
    initialState,
    reducers: {
        set: (state, action) => action.payload,

        addVideo: (state, action: PayloadAction<Video[]>) => {
            // localStorage.setItem('video', JSON.stringify([...state, action.payload]));
            return state.push(action.payload);
        },
    }
})

export const { set: setVideos, } = videosSlice.actions

export const selectVideos = (state: RootState): Video[] => state.videos

export default videosSlice.reducer
