import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { createLogger } from 'redux-logger'
import videos, { Video } from './videos.duck'

const logger = createLogger({ diff: true })

const rootReducer = combineReducers({
    videos,
})

export type RootState = ReturnType<typeof rootReducer>

const store = configureStore({
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
  reducer: rootReducer,
})

export type AppDispatch = typeof store.dispatch

if (process.env.NODE_ENV === 'development') {
    window.store = store
}

export default store
