import React, { useEffect, useRef } from 'react';
import videojs from 'video.js'
import 'video.js/dist/video-js.css'

/** 
 * @todo https://github.com/videojs/videojs-youtube
 * @todo https://github.com/videojs/videojs-vimeo 
 */

export const VideoPlayer = (props) => {
    const videoNode = useRef(null)
    const video = useRef()

    useEffect(() => {
        video.current = videojs(videoNode.current, props, function onPlayerReady() {
            console.log('onPlayerReady')
        })

        return () => {
            video?.current && video.current.dispose()
        }
    }, [])

    return <div data-vjs-player>
        <video id="video" ref={videoNode} className="video-js"></video>
    </div>
}