import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Table } from '../modules/database/airtable';
import { selectVideos, setVideos, Video, } from '../modules/store/videos.duck';
import { AppDispatch } from '../modules/store';
import { VideoPlayer } from './VideoPlayer';

const Videos = new Table('Videos')

let count = 1

interface StateProps {
    videos: Video[];
}

interface DispatchProps {
    set: (videos: Video[]) => void;
}

type AppProps = StateProps & DispatchProps

const App = ({ videos, set }: AppProps) => {
    console.log(`render ${count++}`, videos)

    useEffect(() => {
        Videos.getRecords({
            // view: 'Calendar',
            sort: [{ field: "Date", direction: "asc" }]
        })
            .then((videos) => set(videos.map(({ _rawJson }) => _rawJson)))

        // Videos.getRecord(TRANSACTION_ID)
    }, [])

    return <>
        <VideoPlayer controls={true} /*sources={[{ src: 'https://player.vimeo.com/video/393721338?dnt=1&app_id=122963&background=1', type: 'video/mp4' }]}*/ />
    </>
}

const mapStateToProps = (state: StateProps) => ({
    videos: selectVideos(state),
});

const mapDispatchToProps = (dispatch: AppDispatch): DispatchProps => ({
    set: (videos) => dispatch(setVideos(videos)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
