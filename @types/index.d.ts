import store from '../src/modules/store'

declare global {
  interface Window {
    store?: typeof store;
  }
}
